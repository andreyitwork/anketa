<?php session_start(); ?>                                                                            
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Анкета</title>
        <!-- Подключение jquery и bootstrap-->
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <!-- Разбивка сетки в bootstrap-->
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <?php

                    require_once 'route.php';                                    //подключаем роутер

                ?>
                <!-- Модальное окно через кнопку-->
                <button class="btn btn-default btn-xs" type="button" data-toggle="modal" data-target="#myModal">Не нажимать!</button>
                <div id="myModal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal">×</button>
                                <h6 class="modal-title"><p class="text-center">Подсказка</p></h6>
                            </div>
                            <div class="modal-body">Братка! Не отвлекайся, работай с анкетой!</div>
                            <div class="modal-footer">
                                <button class="btn btn-default btn-xs" type="button" data-dismiss="modal">Закрыть</button></div>
                        </div>
                    </div>
                </div>            
            </div>
            
            <div class="col-md-7">
                <!-- Работа с изображениями-->
                <img src="1.jpg" class="img-rounded">
                <div>
                    <!-- Навигация по вкладкам-->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Анкета</a></li>
                        <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Содержание</a></li>
                    </ul>
                    <!-- Содержимое вкладок -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">Анкета (фр. enquête «расследование; опрос»; ср.: 
                            англ. inquiry) — согласно изданному в 1910 году в Российской империи «Словарю иностранных слов, 
                            вошедших в состав русского языка» Александра Николаевича Чудинова есть «справка по вопросам социальным 
                            или экономическим, доставляемая по требованию правительства сведущими людьми»</div>
                        <div role="tabpanel" class="tab-pane" id="profile">С использованием данной анкеты мы изучим предпочтения 
                            автомобилистов в выборе автомобилей немецкого производства, укомплектованных соответствующими опциями</div>
                    </div>
                </div>
            </div>
        </div>        
    </body>
</html>


