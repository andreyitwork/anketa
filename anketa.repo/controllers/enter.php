<?php
//If the 'Вход' button is pressed (если кнопка "Вход" нажата)

$validateformview->validate_form('enter',$anketa);
$errors = $validateformview->errors;   
if ($errors){                                                               //if errors found (Если при входе в форме обнаружены ошибки)      
    new enterview ($anketa,$errors);        
} else {                                                                    //if no errors found (Если при входе все поля заполнены правильно)        
    $mysqlimodel->mysqli_query(null, $anketa);
    $result=$mysqlimodel->result;
    $anketa = $result;       
    if (empty($result['id'])) {                                             // if user is not exist (если id пользователя в БД пустое, т.е. данных о пользователе нет)               
        echo 'Зарегистрируйтесь пожалуйста!','<br>';
        new renderanketaview($anketa, $errors);            
    } else {                                                                // if user is not exist (если id пользователя в БД не пустое, т.е. данные о пользователе присутствуют)            
        echo "Добро пожаловать, $anketa[name]!";
        new tableview($anketa);
        $_SESSION = $anketa;           
    }           
}
        
 
