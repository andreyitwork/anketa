<?php

class enterview
{

    public function __construct ($data, $errors)                                // function that outputs form 'Enter' (функция, которая выводит страницу входа)
    {
        $output = '<form role="form" class="form-inline" action=""  method="post">';
        $output .= '<p><h3>Вход в систему</h3>';
        if(!empty($errors['name'])){
            $output .= '<code>'. $errors['name'] . '</code></br>';
            $output .= '<div class="form-group has-error">';
        } else {
            $output .= '<div class="form-group">';
        }
        $output .= '<p><input type="text" class="form-control" name="name" value="'. (!empty($data['name']) ? $data['name'] : '') .'">Имя</p>';
        $output .= '</div></br>';
        if(!empty($errors['password'])){
            $output .= '<code>'. $errors['password'] . '</code></br>';
            $output .= '<div class="form-group has-error">';
        } else {
            $output .= '<div class="form-group">';
        }
        $output .= '<p><input type="password" class="form-control" name="password" value="'. (!empty($data['password']) ? $data['password'] : '') .'">Пароль</p>';
        $output .= '</div>';
        $output .= '<p><input type="submit" class="btn btn-primary" name="enter" value="Вход">';
        $output .= '<input data-toggle="tooltip" data-placement="right" title="Перейти к регистрации" type="submit" class="btn btn-info" name="registration" value="Регистрация"></p>';
        $output .= '</form>';
        print $output;
    }
}
