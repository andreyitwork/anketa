<?php

class renderanketaview 
{
      
    public function __construct($data, $errors)                                 // function that displays a form 'Anketa'
    {                    
    $output = '<form role="form" class="form-inline" action=""  method="post">';
    $output .= '<p><h3>Анкета</h3>';
    if(!empty($errors['name'])){
        $output .= '<code>'. $errors['name'] . '</code></br>';
        $output .= '<div class="form-group has-error">';
    } else {
        $output .= '<div class="form-group">';
    }
    $output .= '<p><input type="text" class="form-control" name="name" value="'. (!empty($data['name']) ? $data['name'] : '') .'">Имя</p>';
    $output .= '</div></br>';
    if(!empty($errors['password'])){
        $output .= '<code>'. $errors['password'] . '</code></br>';
        $output .= '<div class="form-group has-error">';
    } else {
        $output .= '<div class="form-group">';
    }
    $output .= '<p><input type="password" class="form-control" name="password" value="'. (!empty($data['password']) ? $data['password'] : '') .'">Пароль</p>';
    $output .= '</div></br>';
    if(!empty($errors['car'])){
        $output .= '<code>'. $errors['car'] . '</code></br>';
        $output .= '<div class="has-error">';
        ;
    } else {
        $output .= '<div class="has-success">';
    }
    $cars_exists=!empty($data['car']);
    $cars = is_array($data['car']) ? $data['car'] : explode(", " , $data['car']);
    $output .= '<div class="checkbox">';
    $output .= '<p>Мои тачки:<input type="checkbox" name="car[]" value="БМВ" '. ($cars_exists && in_array('БМВ',$cars) ? 'checked' : '') .'> БМВ';     // in_array('БМВ',$cars) checks whether a value (БМВ) is present in the array ($data['car'])
    $output .= '<input type="checkbox" name="car[]" value="Мерседес" '. ($cars_exists && in_array('Мерседес',$cars) ? 'checked' : '') .'> Мерседес';
    $output .= '<input type="checkbox" name="car[]" value="Фольксваген" '. ($cars_exists && in_array('Фольксваген',$cars) ? 'checked' : '') .'> Фольксваген</p>';   
    $output .= '</div></div>';
    $output .= '<p>Предпочитаемая КПП:<label><input type="radio" name="gearbox" value="Ручная">Ручная</label>';
    $output .= '<label><input type="radio" name="gearbox" value="Вариатор">Вариатор</label>';
    $output .= '<label><input type="radio" name="gearbox" value="Автоматическая" checked>Автоматическая</label></p>';
    $output .= '<p>Опции:<select name="options" size=1>';
    $output .= '<option value="Кожа">Кожа</option>';
    $output .= '<option value="Литые диски" selected>Литые диски</option>';
    $output .= '<option value="Подогрев руля">Подогрев руля</option>';
    $output .= '<option value="Парктроник">Парктроник</option>';
    $output .= '</select></p>';
    $output .= '<p><input type="submit" class="btn btn-primary" name="submit" value="Старт"></p>';
    $output .= '<p><input type="submit" class="btn btn-warning btn-sm" name="exit" value="Выход">';
    $output .= '</form>';
    print $output; 
    }    
}
