<?php

class tableview 
{
    
    public function __construct($data)                                            // function that displays a table
    {    

        $name=$data['name'];
        $password=$data['password']; 
        $car= is_array($data['car']) ? implode(", " , $data['car']) : $data['car'];
        $gearbox=$data['gearbox'];    
        $options=$data['options'];     

        ?>                    
            <table class="table table-hover table-bordered table-condensed">                                    
                <caption class="text-primary">Таблица вывода данных</caption>
                <tr><th>Параметр</th><th>Введенные данные</th></tr>
                <tr class="success"><td>Ваше имя</td><td><?php echo $name; ?></td></tr>
                <tr class="info"><td>Ваш пароль</td><td><?php echo $password; ?></td></tr>
                <tr class="warning"><td>Ваши тачки</td><td><?php echo $car; ?></td></tr> 
                <tr class="danger"><td>Ваша КПП</td><td><?php echo $gearbox; ?></td></tr>
                <tr class="active"><td>Ваши опции</td><td><?php echo $options; ?></td></tr>
            </table>           
        <?php                                                                // implode(", " , $data['car']) brings together elements of an array ($data['car']) into a string and divide them " ,"
        
        $output = '<form action=""  method="post">';        
        $output .= '<p><input type="submit" class="btn btn-success" name="registration" value="Редактировать данные">';
        $output .= '<input type="submit" class="btn btn-danger btn-xs" name="delete" value="Удалить данные"></p>';
        $output .= '<p><input type="submit" class="btn btn-warning btn-sm" name="exit" value="Выход"></p>';
        $output .= '</form>';
        print $output;
    }
}
