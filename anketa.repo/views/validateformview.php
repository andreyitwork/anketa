<?php

class validateformview
{
    public $errors;                                                             // Property that contains errors
    
    public function validate_form($form, $data)                                 // function that checks the correctness of filling the form
    {
        if ($form=='enter'){
            $this->validate_enter($data);
        } elseif ($form=='anketa'){
            $this->validate_anketa($data);
        }
    }
        
    private function validate_enter($data)
    {                                                                           // function that checks the correctness of filling the form 'Enter'
    $errors = array();
    if(empty($data['name'])){                                                   // if field name is empty (если поле "Имя" пустое)
        $errors['name'] = 'Field "Имя" is empty';                               // transfer to the array $errors value 'Field name is empty' with key 'name'
    } elseif (strlen($data['name']) < 5){                                       // if symbols length of entered 'name' less than 5 symbols
        $errors['name'] = 'Too short name. Please inter 5+ symbols';            // transfer to the array $errors value 'Too short name. Please inter 5+ symbols' with key 'name'
    }
    if(empty($data['password'])){
        $errors['password'] = 'Field "Пароль" is empty';
    } elseif (strlen($data['password']) < 5){
        $errors['password'] = 'Too short password. Please inter 5+ symbols';
    }       
    $this->errors = $errors;
    }   
        
    private function validate_anketa($data)                                     // function that checks the correctness of filling the form 'Anketa'
    {                                          
        
    if(empty($data['name'])){                                                   // if field name is empty (если поле "Имя" пустое)
        $errors['name'] = 'Field "Имя" is empty';                               // transfer to the array $errors value 'Field name is empty' with key 'name'
    } elseif (strlen($data['name']) < 5){                                       // if symbols length of entered 'name' less than 5 symbols
        $errors['name'] = 'Too short name. Please inter 5+ symbols';            // transfer to the array $errors value 'Too short name. Please inter 5+ symbols' with key 'name'
    }
    if(empty($data['password'])){
        $errors['password'] = 'Field "Пароль" is empty';
    } elseif (strlen($data['password']) < 5){
        $errors['password'] = 'Too short password. Please inter 5+ symbols';
    }         
    if(empty($data['car'])){
        $errors['car'] = 'Field "Мои тачки" is empty';
    }    
    $this->errors = $errors;
    }  
    
}
